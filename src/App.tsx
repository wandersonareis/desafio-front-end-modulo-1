import { BrowserRouter as Router, useRoutes } from "react-router-dom";

import Home from "./pages/Home";
import Portfolio from "./pages/Portfolio";
import PortfolioManage from "./pages/portfolios/PortfolioManage";
import PortfolioBookmark from "./pages/portfolios/PortfolioBookmark"
import PortfolioFylo from "./pages/portfolios/PortfolioFylo";
import PortfolioInsure from "./pages/portfolios/PortfolioInsure";
import Contato from "./pages/Contato";
import Header from "./components/header";
import Footer from "./components/footer";

import "./styles/styles.scss";

function AppRoutes() {
  const routes = useRoutes([
    { path: "/desafio-front-end-modulo-1", element: <Home /> },
    { path: "/portfolio", element: <Portfolio /> },
    { path: "/portfolio/manage", element: <PortfolioManage /> },
    { path: "/portfolio/bookmark", element: <PortfolioBookmark /> },
    { path: "/portfolio/fylo", element: <PortfolioFylo /> },
    { path: "/portfolio/insure", element: <PortfolioInsure /> },
    { path: "/contato", element: <Contato /> },
  ]);
  return routes;
}

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <AppRoutes />
        <Footer />
      </Router>
    </div>
  );
}

export default App;
