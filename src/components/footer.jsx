import { Link } from "react-router-dom";

import logoLight from "../assets/logo-light.svg";
import githubLight from "../assets/icons/github-light.svg";
import twitterLight from "../assets/icons/twitter-light.svg";
import linkedinLight from "../assets/icons/linkedin-light.svg";

export default function footer() {
  return (
    <footer aria-label="Rodapé da página">
      <nav
        className="footer-container"
        aria-label="Menu de navegação no rodapé"
      >
        <div className="ft-left">
          <img
            className="show-img-880"
            src={logoLight}
            alt="logo"
            aria-hidden="true"
          />
          <ul
            role="menubar"
            className="horizontal-list"
            aria-label="Menu redirecionando para as página Principal, portifólio e contato"
          >
            <li role="none">
              <Link
                role="menuitem"
                className="btn-link-footer"
                to={"/desafio-front-end-modulo-1"}
                aria-label="Ir para a página principal"
              >
                home
              </Link>
            </li>
            <li role="none">
              <Link
                role="menuitem"
                className="btn-link-footer"
                to={"/portfolio"}
                aria-label="Ir para o portifólio"
              >
                portfolio
              </Link>
            </li>
            <li role="none">
              <Link
                role="menuitem"
                className="btn-link-footer"
                to={"/contato"}
                aria-label="Ir para a página de contato"
              >
                contato
              </Link>
            </li>
          </ul>
        </div>
        <ul
          role="menu"
          className="horizontal-list"
          aria-label="Menu com minhas redes sociais"
        >
          <li role="menuitem">
            <img
              src={githubLight}
              alt="github logo"
              aria-label="Meu portifólio no Github"
            />
          </li>
          <li role="menuitem">
            <img
              src={twitterLight}
              alt="twiter logo"
              aria-label="Meu perfil no Twitter"
            />
          </li>
          <li role="menuitem">
            <img
              src={linkedinLight}
              alt="linkeding logo"
              aria-label="Minha página no Linkedin"
            />
          </li>
        </ul>
      </nav>
    </footer>
  );
}
