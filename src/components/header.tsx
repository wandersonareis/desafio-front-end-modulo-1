import { NavLink } from "react-router-dom";

import logoDark from "../assets/logo-dark.svg";

export default function header() {
  return (
    <header aria-label="Topo da página">
      <nav className="nav-bar__header" aria-label="Menu de navegação principal">
        <img
          className="show-img-880"
          aria-hidden="true"
          src={logoDark}
          alt="logo"
        />
        <ul
          role="menubar"
          className="itens__nav-bar"
          aria-label="Menu redirecionando para as página Principal, portifólio e contato"
        >
          <li role="none">
            <NavLink
              role="menuitem"
              className={({ isActive }) =>
                isActive ? "current-link" : "btn-link-header"
              }
              to={"/desafio-front-end-modulo-1"}
              aria-label="Ir para a página principal"
            >
              Home
            </NavLink>
          </li>
          <li role="none">
            <NavLink
              role="menuitem"
              className={({ isActive }) =>
                isActive ? "current-link" : "btn-link-header"
              }
              to={"/portfolio"}
              aria-label="Ir para o portifólio"
            >
              portfolio
            </NavLink>
          </li>
          <li role="none">
            <NavLink
              role="menuitem"
              className={({ isActive }) =>
                isActive ? "current-link" : "btn-link-header"
              }
              to={"/contato"}
              aria-label="Ir para a página de contato"
            >
              contato
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
