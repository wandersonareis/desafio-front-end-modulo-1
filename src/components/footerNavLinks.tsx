import { Link } from "react-router-dom";

export type FooterNavProps = {
  beforeText: string;
  beforeUrl: string;
  afterText: string;
  afterUrl: string;
};

export default function footerNavLinks(props: FooterNavProps) {
  const { beforeText, beforeUrl, afterText, afterUrl } = props;
  return (
    <div className="footer-links">
      <section>
        <div className="links-container">
          <div className="links">
            <Link to={beforeUrl} role="link">
              <h1 className="heading-mid">{beforeText}</h1>
            </Link>
            <span className="main-2">Projeto Anterior</span>
          </div>
          <div className="links">
            <Link to={afterUrl} role="link">
              <h1 className="heading-mid">{afterText}</h1>
            </Link>
            <span className="main-2">Próximo projeto</span>
          </div>
        </div>
      </section>
    </div>
  );
}
