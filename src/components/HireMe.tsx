import { Link } from "react-router-dom";

export default function HireMe() {
  return (
    <section>
      <div className="contact-container">
        <h2 className="heading-mid">Interessado em fazer projetos comigo?</h2>
        <Link className="main-2 btn-contact" to={"/contato"}>
          contato
        </Link>
      </div>
    </section>
  );
}
