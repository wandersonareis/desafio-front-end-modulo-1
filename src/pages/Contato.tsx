import githubDark from "../assets/icons/github-dark.svg";
import twitterDark from "../assets/icons/twitter-dark.svg";
import linkedinDark from "../assets/icons/linkedin-dark.svg";

export default function Contato() {
  return (
    <main className="main__flex-box">
      <section id="contacte-me-section" className="border-top border-bottom">
        <div className="contact-left-column">
          <h2 className="heading-mid">Entre em Contato</h2>
        </div>
        <div className="contact-right-column">
          <p className="main-1 justify">
            Eu adoraria escutar sobre seu trabalho atual e como eu poderia
            ajudar. Atualmente estou procurando por um cargo e aberto para
            várias oportunidades. Minha preferência é uma oportunidade em uma
            empresa Britânica. Mas também estou feliz em escutar sobre
            oportunidades remotas. Sou uma pessoa trabalhadora e positiva que
            sempre fará as tasks com um senso de propósito e atenção aos
            detalhes. Fique livre para checar meu perfis abaixo e entrar em
            contato utilizando o formulário.
          </p>
          <ul className="horizontal-list">
            <li>
              <img
                src={githubDark}
                alt="github logo"
                aria-label="Meu portifólio no Github"
              />
            </li>
            <li>
              <img
                src={twitterDark}
                alt="twiter logo"
                aria-label="Meu perfil no Twitter"
              />
            </li>
            <li>
              <img
                src={linkedinDark}
                alt="linkeding logo"
                aria-label="Minha página no Linkedin"
              />
            </li>
          </ul>
        </div>
      </section>
      <section id="section-form">
        <div className="contact-left-column">
          <h2 className="heading-mid">Contato</h2>
        </div>
        <div className="contact-right-column">
          <form>
            <label className="main-2">Nome Completo</label>
            <input
              title="Nome completo"
              type="text"
              name="name"
              id="txtName"
              placeholder="Digite o seu nome..."
              aria-label="Insira seu nome completo"
              translate="no"
            />

            <label className="main-2">E-mail</label>
            <input
              title="Email"
              type="email"
              name="email"
              id="txtEmail"
              placeholder="Digite o seu e-mail..."
              aria-label="Coloque seu email de contato"
              translate="no"
            />

            <label className="main-2">Mensagem</label>
            <textarea
              title="Digite a menssagem"
              name="message"
              placeholder="Digite a sua mensagem..."
              id="txtMessage"
              aria-label="Digitar a menssagem que quer enviar"
            ></textarea>

            <button type="submit" id="btnSubmit" className="btn-send">
              Enviar
            </button>
          </form>
        </div>
      </section>
    </main>
  );
}
