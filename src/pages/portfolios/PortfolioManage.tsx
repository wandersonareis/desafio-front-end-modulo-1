import { Link } from "react-router-dom";

import HireMe from "../../components/HireMe";

import imageManageHero from "../../assets/portfolio/manage/image-manage-hero.jpg";
import imageManagePreview_1 from "../../assets/portfolio/manage/image-manage-preview-1.jpg";
import imageManagePreview_2 from "../..//assets/portfolio/manage/image-manage-preview-2.jpg";
import FooterNavLinks from "../../components/footerNavLinks";

export default function PortfolioManage() {
    return (
        <main>
          <div className="main__flex-box">
            <section>
              <div className="main-container">
                <img
                  src={imageManageHero}
                  alt="Imagem da tela principal"
                  aria-label="Ima da tela principal do projeto Manager"
                />
              </div>
              <div className="project-container">
                <div className="project-text left-column">
                  <h2 className="heading-mid border-top">Manage</h2>
                  <p className="main-1">
                    Esse projeto me fez criar uma landing page responsiza de
                    acordo com o design que recebi. Usei HTML5, CSS Grid e
                    JavaScript para as áreas interativas, como o slider de
                    testimoniais.
                  </p>
                  <div className="interaction-links">
                    <Link role={"link"} className="main-1" to={""}>
                      Interaction Design / Front End Development
                    </Link>
                    <Link role={"link"} className="main-1" to={""}>
                      HTML / CSS / JS
                    </Link>
                  </div>
                  <Link
                    role="button"
                    className="main-2 btn-primary"
                    to={"/portfolio/manage"}
                  >
                    visitar
                  </Link>
                </div>
                <div className="project-text">
                  <h2 className="heading-sm">Projeto</h2>
                  <span className="main-2">
                    Este projeto foi um desafio de front-end do Frontend Mentor.
                    É uma plataforma que te faz práticar construindo websites a
                    partir de um design e casos de usuário. Cada desafio contém
                    designs mobile e desktop para ilustrar como o website seria
                    em diferentes tamanhos de tela. Criar esses projetos me
                    ajudou a refinar meu fluxo de trabalho e resolver problemas
                    de código da vida real. Eu aprendi algo novo com cada
                    projeto, me ajudando a melhorar e adaptar meu estilo.
                  </span>
                  <h2 className="heading-sm">Previews Estáticos</h2>
                  <img
                    src={imageManagePreview_1}
                    alt="manage project preview 1"
                  />
                  <img
                    src={imageManagePreview_2}
                    alt="manage project preview 2"
                  />
                </div>
              </div>
            </section>
          </div>
          <FooterNavLinks
            beforeText="Fylo"
            beforeUrl="/portfolio/fylo"
            afterText="Bookmark"
            afterUrl="/portfolio/bookmark"
          />
          <HireMe />
        </main>
    );
  }
