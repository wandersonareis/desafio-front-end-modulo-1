import HireMe from "../../components/HireMe";

import imageInsureHero from "../../assets/portfolio/insure/image-insure-hero.jpg";
import imageInsurePreview_1 from "../../assets/portfolio/insure/image-insure-preview-1.jpg";
import imageInsurePreview_2 from "../..//assets/portfolio/insure/image-insure-preview-2.jpg";
import { Link } from "react-router-dom";
import FooterNavLinks from "../../components/footerNavLinks";

export default function PortfolioInsure() {
  return (
    <main>
      <div className="main__flex-box">
        <section>
          <div className="main-container">
            <img
              src={imageInsureHero}
              alt="Imagem da tela principal"
              aria-label="Ima da tela principal do projeto Insure"
            />
          </div>
          <div className="project-container">
            <div className="project-text left-column">
              <h2 className="heading-mid border-top">Insure</h2>
              <p className="main-1">
                Este foi um projeto pequeno que consistiu em HTML e CSS
                principalmente. Eu construí uma landing page totalmente
                responsiva. O único JavaScript que usei foi para o menu de
                navegação mobile.
              </p>
              <div className="interaction-links">
                <Link role={"link"} className="main-1" to={"/"}>
                  Interaction Design / Front End Development
                </Link>
                <Link role={"link"} className="main-1" to={"/"}>
                  HTML / CSS / JS
                </Link>
              </div>
              <Link
                role="button"
                className="main-2 btn-primary"
                to={"//portfolio/insure"}
              >
                visitar
              </Link>
            </div>
            <div className="project-text">
              <h2 className="heading-sm">Projeto</h2>
              <span className="main-2">
                Este projeto foi um desafio de front-end do Frontend Mentor. É
                uma plataforma que te faz práticar construindo websites a partir
                de um design e casos de usuário. Cada desafio contém designs
                mobile e desktop para ilustrar como o website seria em
                diferentes tamanhos de tela. Criar esses projetos me ajudou a
                refinar meu fluxo de trabalho e resolver problemas de código da
                vida real. Eu aprendi algo novo com cada projeto, me ajudando a
                melhorar e adaptar meu estilo.
              </span>
              <h2 className="heading-sm">Previews Estáticos</h2>
              <img src={imageInsurePreview_1} alt="insure project preview 1" />
              <img src={imageInsurePreview_2} alt="insure project preview 2" />
            </div>
          </div>
        </section>
      </div>
      <FooterNavLinks
        beforeText="Bookmark"
        beforeUrl="/portfolio/bookmark"
        afterText="Fylo"
        afterUrl="/portfolio/fylo"
      />
      <HireMe />
    </main>
  );
}
