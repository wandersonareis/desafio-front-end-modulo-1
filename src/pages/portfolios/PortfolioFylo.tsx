import { Link } from "react-router-dom";

import HireMe from "../../components/HireMe";

import imagefyloHero from "../../assets/portfolio/fylo/image-fylo-hero.jpg";
import imagefyloPreview_1 from "../../assets/portfolio/fylo/image-fylo-preview-1.jpg";
import imagefyloPreview_2 from "../..//assets/portfolio/fylo/image-fylo-preview-2.jpg";
import FooterNavLinks from "../../components/footerNavLinks";

export default function PortfolioFylo() {
  return (
    <main>
      <div className="main__flex-box">
        <section>
          <div className="main-container">
            <img
              src={imagefyloHero}
              alt="Imagem da tela principal"
              aria-label="Ima da tela principal do projeto Fylo"
            />
          </div>
          <div className="project-container">
            <div className="project-text left-column">
              <h2 className="heading-mid border-top">Fylo</h2>
              <p className="main-1">
                Este projeto foi puramente HTML e CSS. Eu recebi designs mobile
                e desktop para construir, então ele foi totalmente responsivo.
                Eu tomei um caminho mobile-first e usei CSS moderno como Flexbox
                e Grid para criar o layout.
              </p>
              <div className="interaction-links">
                <Link role={"link"} className="main-1" to={"/"}>
                  Interaction Design / Front End Development
                </Link>
                <Link role={"link"} className="main-1" to={"/"}>
                  HTML / CSS / JS
                </Link>
              </div>
              <Link
                role="button"
                className="main-2 btn-primary"
                to={"/portfolio/fylo"}
              >
                visitar
              </Link>
            </div>
            <div className="project-text">
              <h2 className="heading-sm">Projeto</h2>
              <span className="main-2">
                Este projeto foi um desafio de front-end do Frontend Mentor. É
                uma plataforma que te faz práticar construindo websites a partir
                de um design e casos de usuário. Cada desafio contém designs
                mobile e desktop para ilustrar como o website seria em
                diferentes tamanhos de tela. Criar esses projetos me ajudou a
                refinar meu fluxo de trabalho e resolver problemas de código da
                vida real. Eu aprendi algo novo com cada projeto, me ajudando a
                melhorar e adaptar meu estilo.
              </span>
              <h2 className="heading-sm">Previews Estáticos</h2>
              <img
                src={imagefyloPreview_1}
                alt="Fylo preview 1"
                aria-label="Imagem da tela principal do projeto Fylo"
              />
              <img
                src={imagefyloPreview_2}
                alt="Fylo preview 2"
                aria-label="Imagem com as telas do projeto Fylo"
              />
            </div>
          </div>
        </section>
      </div>
      <FooterNavLinks
        beforeText="Insure"
        beforeUrl="/portfolio/insure"
        afterText="Manage"
        afterUrl="/portfolio/manage"
      />
      <HireMe />
    </main>
  );
}
