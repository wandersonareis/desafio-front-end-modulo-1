import { Link } from "react-router-dom";

import imagebookmarkHero from "../../assets/portfolio/bookmark/image-bookmark-hero.jpg";
import imagebookmarkPreview_1 from "../../assets/portfolio/bookmark/image-bookmark-preview-1.jpg";
import imagebookmarkPreview_2 from "../..//assets/portfolio/bookmark/image-bookmark-preview-2.jpg";
import FooterNavLinks from "../../components/footerNavLinks";
import HireMe from "../../components/HireMe";

export default function PortfolioBookmark() {
  return (
    <main>
      <div className="main__flex-box">
        <section>
          <div className="main-container">
            <img
              src={imagebookmarkHero}
              alt="Imagem da tela principal"
              aria-label="Ima da tela principal do projeto Bookmark"
            />
          </div>
          <div className="project-container">
            <div className="project-text left-column">
              <h2 className="heading-mid border-top">Bookmark</h2>
              <p className="main-1">
                Esse projeto me fez criar uma landing page responsiza de acordo
                com o design que recebi. Usei HTML5, CSS Grid e JavaScript para
                as áreas interativas, como a área de Features.
              </p>
              <div className="interaction-links">
                <Link role={"link"} className="main-1" to={"/"}>
                  Interaction Design / Front End Development
                </Link>
                <Link role={"link"} className="main-1" to={"/"}>
                  HTML / CSS / JS
                </Link>
              </div>
              <Link
                role="button"
                className="main-2 btn-primary"
                to={"/portfolio/bookmark"}
              >
                visitar
              </Link>
            </div>
            <div className="project-text">
              <h2 className="heading-sm">Projeto</h2>
              <span className="main-2">
                Este projeto foi um desafio de front-end do Frontend Mentor. É
                uma plataforma que te faz práticar construindo websites a partir
                de um design e casos de usuário. Cada desafio contém designs
                mobile e desktop para ilustrar como o website seria em
                diferentes tamanhos de tela. Criar esses projetos me ajudou a
                refinar meu fluxo de trabalho e resolver problemas de código da
                vida real. Eu aprendi algo novo com cada projeto, me ajudando a
                melhorar e adaptar meu estilo.
              </span>
              <h2 className="heading-sm">Previews Estáticos</h2>
              <img
                src={imagebookmarkPreview_1}
                alt="Imagem da tela principal do projeto Bookmark"
              />
              <img
                src={imagebookmarkPreview_2}
                alt="Imagem com as telas do projeto Bookmark"
              />
            </div>
          </div>
        </section>
      </div>
      <FooterNavLinks
        beforeText="Manage"
        beforeUrl="/portfolio/manage"
        afterText="Insure"
        afterUrl="/portfolio/insure"
      />
      <HireMe />
    </main>
  );
}
