import { Link } from "react-router-dom";

import HireMe from "../components/HireMe";

import imagePortfolioManage from "../assets/portfolio/manage/image-portfolio-manage.jpg";
import imagePortfolioBookmark from "../assets/portfolio/bookmark/image-portfolio-bookmark.jpg";
import imagePortfolioInsure from "../assets/portfolio/insure/image-portfolio-insure.jpg";
import imagePortfolioFylo from "../assets/portfolio/fylo/image-portfolio-fylo.jpg";

export default function Portfolio() {
  return (
    <main className="main__flex-box">
      <section>
        <div className="main__flex-box row">
          <div className="portfolio-content col-1">
            <img
              src={imagePortfolioManage}
              alt="Imagem portfolio"
              aria-label="Imagem do portifólio Manager"
            />
          </div>
          <div className="portfolio-text col-2">
            <h2 className="heading-mid portfolio-title">Manage</h2>
            <p className="main-1 justify">
              Esse projeto me fez criar uma landing page responsiza de acordo
              com o design que recebi. Usei HTML5, CSS Grid e JavaScript para as
              áreas interativas, como o slider de testimoniais.
            </p>
            <Link
              role="button"
              className="main-2 btn-primary"
              to={"/portfolio/manage"}
            >
              ver projeto
            </Link>
          </div>
        </div>
      </section>
      <section>
        <div className="main__flex-box row">
          <div className="portfolio-text col-3">
            <h2 className="heading-mid portfolio-title">Bookmark</h2>
            <p className="main-1 justify">
              Esse projeto me fez criar uma landing page responsiza de acordo
              com o design que recebi. Usei HTML5, CSS Grid e JavaScript para as
              áreas interativas, como a área de Features.
            </p>
            <Link
              role="button"
              className="main-2 btn-primary"
              to={"/portfolio/bookmark"}
            >
              ver projeto
            </Link>
          </div>
          <div className="portfolio-content col-4">
            <img src={imagePortfolioBookmark} alt="" />
          </div>
        </div>
      </section>
      <section>
        <div className="main__flex-box row">
          <div className="portfolio-content col-5">
            <img src={imagePortfolioInsure} alt="" />
          </div>
          <div className="portfolio-text col-6">
            <h2 className="heading-mid portfolio-title">Insure</h2>
            <p className="main-1 justify">
              Este foi um projeto pequeno que consistiu em HTML e CSS
              principalmente. Eu construí uma landing page totalmente
              responsiva. O único JavaScript que usei foi para o menu de
              navegação mobile.
            </p>
            <Link
              role="button"
              className="main-2 btn-primary"
              to={"/portfolio/insure"}
            >
              ver projeto
            </Link>
          </div>
        </div>
      </section>
      <section>
        <div className="main__flex-box row">
          <div className="portfolio-text col-7">
            <h2 className="heading-mid portfolio-title">Fylo</h2>
            <p className="main-1 justify">
              Este projeto foi puramente HTML e CSS. Eu recebi designs mobile e
              desktop para construir, então ele foi totalmente responsivo. Eu
              tomei um caminho mobile-first e usei CSS moderno como Flexbox e
              Grid para criar o layout.
            </p>
            <Link
              role="button"
              className="main-2 btn-primary"
              to={"/portfolio/fylo"}
            >
              ver projeto
            </Link>
          </div>
          <div className="portfolio-content col-8">
            <img src={imagePortfolioFylo} alt="" />
          </div>
        </div>
      </section>
      <HireMe />
    </main>
  );
}
