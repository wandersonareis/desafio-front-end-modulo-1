import { Link } from "react-router-dom";

import HireMe from "../components/HireMe";

import imageHomePageHero from "../assets/home/image-homepage-hero.jpg";
import imageHomePageProfile from "../assets/home/image-homepage-profile.jpg";

export default function Home() {
  return (
    <main className="main__flex-box">
      <section>
        <div className="perfil-container">
          <img src={imageHomePageHero} alt="meu perfil" />
          <h1 className="heading-big bottom-left">
            Olá, me chamo Alex Spencer e eu amo construir websites lindos.
            <Link
              role="button"
              className="main-2 icon-btn btn-home-overlay"
              // href="#about"
              to={""}
            >
              <span className="button__icon"></span>
              <span className="button__text">sobre mim</span>
            </Link>
          </h1>
        </div>
      </section>
      <section id="about">
        <div className="about-container">
          <div className="about-content" aria-label="Minha foto de perfil">
            <img src={imageHomePageProfile} alt="" />
          </div>
          <div className="about-text" aria-label="Um pouco sobre mim">
            <h2 className="heading-mid about-me-title" aria-label="Sobre mim">
              Sobre mim
            </h2>
            <p className="main-1 justify" aria-label="Um pouco sobre mim">
              Sou um desenvolvedor front-end júnior procurando por uma
              oportunidade. Eu foco em escrever HTML acessível, usando práticas
              modernas de CSS e escrevendo um JavaScript limpo. Quando estou
              escrevendo código JavaScript, na maioria das vezes estou usando
              React, mas posso me adapta para qualquer ferramenta se necessário.
              Moro em Londres, UK, mas também seria feliz trabalhando
              remotamente e tenho experiência em times remotos. Quando não estou
              codando, poderá me achar fora de casa. Eu adoro estar próximo a
              natureza seja para uma caminhada, corrida ou ciclismo. Eu amaria
              se você desse uma olhada no meu trabalho.
            </p>
            <Link
              className="main-2 btn-primary"
              to={"/portfolio"}
              aria-label="Clique para meu portfolio"
            >
              ir para portfolio
            </Link>
          </div>
        </div>
      </section>
      <HireMe />
    </main>
  );
}
